FROM ubuntu:20.04

RUN apt update
RUN apt install -y software-properties-common
RUN add-apt-repository -y ppa:deadsnakes/ppa
RUN apt install -y \
    curl \
    python3.6 \
    python3.7 \
    python3.8 \
    python3-distutils

WORKDIR /tmp
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
RUN python3.6 get-pip.py && \
    python3.7 get-pip.py && \
    python3.8 get-pip.py

RUN pip3.8 install tox

COPY . /save-to-db
WORKDIR /save-to-db
