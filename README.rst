Save-to-DB
----------
This library can be used to store scraped data from the Internet into a
database without much effort. It can create, update and delete records in
database to accommodate new data. It can even configure itself by default by
getting not null, unique and primary key constrains, foreign keys and other info
from the database. It can work with Sqlalchemy and Django ORM models without
much setup. Just throw your data at it and it will take care of storing it
properly in a database.

**Installation**::

    pip install Save-to-DB

----

**Warning:** This library works only with Python 3.

----

|

**Documentation and tutorial:**

- On the Internet: https://save-to-db.readthedocs.io
- Download for offline reading:
  https://readthedocs.org/projects/save-to-db/downloads/


**Project repository:** https://bitbucket.org/mikhail-makovenko/save-to-db