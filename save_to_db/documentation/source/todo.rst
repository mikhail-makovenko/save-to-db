TODO
====

List of TODO:
++++++++++++++

#. Allow using integer keys to get items of BulkItem. Currently only string
   keys are allowed to be used to get default values.
#. Allow assinging relational items using dictionary:

   .. code-block:: python

      some_item = SomeItem(
         # single item
         related_item = {
            "field_a": 10,
         },
         # bulk item
         related_items = [
            {
               "field_a" 1,
            },
            {
               "field_a" 2,
            },
         ],
      )
#. Add support global defaults.
   For example, this will allow to easier track data coming from the same source
   (for example, web-site).

   Example usage:

   .. code-block:: python

      from save_to_db import Item

      class SomeItem(Item):
         # ... omitted code here...

         @classmethods
         def get_global_default(cls, referring_item, referring_key):
            # ... omitted code here...
            return instance

   Another example:

   .. code-block:: python

      from save_to_db import ItemCollection

      # `collection_id=None` for not scoped items:
      collection = ItemCollection.get_collection_by_id(collection_id=None)
      collection.set_global_default(SourceItem(name = "default source"))
   
   Global defaults must be automatically assigned upon item process to all
   items that have x-to-one reference to the item class.

#. Add support for encrypted fields.

#. Use "context" variable during item processing and persistance.

#. Solve empty model deleter bug (does not delete existing models as it does not
   know how to select them).