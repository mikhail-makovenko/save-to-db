Database adapters
=================

.. contents:: Table of Contents

adapter_manager
---------------

.. automodule:: save_to_db.adapters.utils.adapter_manager
    :members:

AdapterBase
-----------

.. autoclass:: save_to_db.adapters.utils.adapter_base.AdapterBase
    :members:

Column and relation types
-------------------------

ColumnType
++++++++++

.. autoclass:: save_to_db.adapters.utils.column_type.ColumnType
    :members:
    :undoc-members:

RelationType
++++++++++++

.. autoclass:: save_to_db.adapters.utils.relation_type.RelationType
    :members:
    :undoc-members:

.. _Adapters:

Adapters
--------

SqlAlchemy adapter
++++++++++++++++++

.. autoclass:: save_to_db.adapters.sqlalchemy_adapter.SqlalchemyAdapter

Django ORM adapter
++++++++++++++++++

.. autoclass:: save_to_db.adapters.django_adapter.DjangoAdapter
