from docutils.parsers.rst import directives
from docutils.parsers.rst.directives.images import Figure


class ScaleFigure(Figure):
    option_spec = Figure.option_spec.copy()
    option_spec["scale-html"] = directives.percentage
    option_spec["scale-latex"] = directives.percentage
    option_spec["scale-epub2"] = directives.percentage
    option_spec["scale-mobi"] = directives.percentage

    def run(self):
        env = self.state.document.settings.env
        builder_name = env.app.builder.name

        self.options["scale"] = self.options.get("scale-" + builder_name, 100)
        self.options["align"] = "center"

        return super().run()


def setup(app):
    app.add_directive("scale_figure", ScaleFigure)
    app.add_config_value("image_dir", "figs", False)
    app.add_config_value("black_and_white", False, True)
    app.add_config_value("image_dir_black_white", "figs-bw", False)
