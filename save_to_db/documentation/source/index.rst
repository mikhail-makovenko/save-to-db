Welcome to Save-To-DB's documentation!
======================================

.. toctree::
    :maxdepth: 3
    :caption: Table of Contents

    introduction
    tutorial
    item
    adapters
    signals
    merge_policy
    mapper
    exceptions
    scrapy_integration
    todo


.. only:: html

    Indices and tables
    ==================

    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`
