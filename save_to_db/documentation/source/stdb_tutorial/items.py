import models
from save_to_db import item_cls_manager, Item

item_cls_manager.autogenerate = True


class ExchangeItem(Item):
    allow_merge_items = True
    model_cls = models.Exchange


class CompanyItem(Item):
    allow_merge_items = True
    model_cls = models.Company


class DividendItem(Item):
    model_cls = models.Dividend


class InstrumentItem(Item):
    allow_merge_items = True
    model_cls = models.Instrument
    getters = ["tickers"]
    getters_autoconfig = True


class TickerItem(Item):
    allow_merge_items = True
    model_cls = models.Ticker
