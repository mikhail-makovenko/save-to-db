import sqlalchemy as sa
from sqlalchemy.orm import relationship
from conf import ModelBase


class Exchange(ModelBase):
    __tablename__ = "exchange"
    name = sa.Column(sa.String(), primary_key=True)


class Company(ModelBase):
    __tablename__ = "company"
    name = sa.Column(sa.String(), primary_key=True)


class Instrument(ModelBase):
    __tablename__ = "instrument"
    isin_code = sa.Column(sa.String(length=12), primary_key=True)
    company_name = sa.Column(
        sa.String(), sa.ForeignKey(Company.name, ondelete="CASCADE"), primary_key=True
    )
    company = relationship(Company, backref="instruments")


class Dividend(ModelBase):
    __tablename__ = "dividend"
    instrument_isin_code = sa.Column(
        sa.String(length=12),
        sa.ForeignKey(Instrument.isin_code, ondelete="CASCADE"),
        primary_key=True,
    )
    instrument = relationship(Instrument, backref="dividends")
    date = sa.Column(sa.Date(), primary_key=True)
    amount = sa.Column(sa.Float(), nullable=False)


class Ticker(ModelBase):
    __tablename__ = "ticker"
    code = sa.Column(sa.String(), primary_key=True)
    exchange_name = sa.Column(
        sa.String(), sa.ForeignKey(Exchange.name, ondelete="CASCADE"), primary_key=True
    )
    exchange = relationship(Exchange, backref="tickers")
    instrument_isin_code = sa.Column(
        sa.String(length=12),
        sa.ForeignKey(Instrument.isin_code, ondelete="CASCADE"),
        nullable=False,
    )
    instrument = relationship(Instrument, backref="tickers")
