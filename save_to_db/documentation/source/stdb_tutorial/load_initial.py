import items
from conf import engine, ModelBase, persister

ModelBase.metadata.create_all(engine)

# The data can come from anywhere, for demontration purpose we hard-coded it
COMPANY_DATA = [
    # COMPANY,                   INSTRUMENT ISIN,    TICKER
    ["Polymetal International", "JE00B6T5S470", "POLY"],
    ["Qiwi Plc", "US74735M1080", "QIWI"],
]
DIVIDEND_DATA = [
    # INSTRUMENT ISIN,   DATE,            AMOUNT
    ["JE00B6T5S470", "2018-05-11", "0.3"],
    ["JE00B6T5S470", "2017-05-05", "0.18"],
    ["US74735M1080", "2017-08-29", "0.21"],
    ["US74735M1080", "2017-05-29", "0.22"],
]

# The exchange on which the company stocks are traded
exchange = items.ExchangeItem(name="Moscow Exchange")
companies = items.CompanyItem.Bulk()
for (company_name, instrument_isin_code, ticker_code) in COMPANY_DATA:
    company = companies.gen(name=company_name)
    # Since each company has many instruments, "instruments" is a bulk item.
    # Items are automatically created when you try to access them.
    instrument = company["instruments"].gen(isin_code=instrument_isin_code)
    instrument["tickers"].gen(exchange=exchange, code=ticker_code)
    # For demontration purposes, loading dividends here
    for (instrument_isin_code, date, amount) in (
        row for row in DIVIDEND_DATA if row[0] == instrument_isin_code
    ):
        instrument["dividends"].gen(date=date, amount=amount)

persister.persist(companies)
persister.commit()
