from save_to_db import Persister
from save_to_db.adapters import SqlalchemyAdapter

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine("sqlite:///database.db", echo=False)
DBSession = sessionmaker(bind=engine)
session = DBSession()
ModelBase = declarative_base()

persister = Persister(SqlalchemyAdapter({"session": session, "ModelBase": ModelBase}))
