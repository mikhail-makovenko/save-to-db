import items
from conf import persister

# Note that there is no instrument data here
DIVIDEND_DATA = [
    # TICKER     DATE,            AMOUNT
    ["QIWI", "2007-03-28", "0.19"],  # new data
    ["QIWI", "2017-05-29", "10.0"],  # data to be updated
]

exchange = items.ExchangeItem(name="Moscow Exchange")
# setting fields on a bulk items sets default values for the items
# (you can also use double underscore to access subfields)
dividends = items.DividendItem.Bulk(instrument__tickers__exchange=exchange)
for (ticker_code, date, amount) in DIVIDEND_DATA:
    dividend = dividends.gen(date=date, amount=amount)
    # using double underscore to access subfields
    dividend["instrument__tickers"].gen(code=ticker_code)

persister.persist(dividends)
persister.commit()
