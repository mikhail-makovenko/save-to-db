Introduction
============

.. contents:: Table of Contents

Installation and requirements
-----------------------------

*Save-to-DB* library can be used to store scraped data from the Internet into a
database without much effort. It can create, update and delete records in
database to accommodate new data. It can even configure itself by default by
getting not null, unique and primary key constrains, foreign keys and other info
from the database. It can work with Sqlalchemy and Django ORM models without
much setup. Just throw your data at it and it will take care of storing it
properly in a database.

You can install *Save-to-DB* library using pip::

    pip install Save-to-DB

**The library only works with Python 3.**
