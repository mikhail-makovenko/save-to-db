Item configuration and usage
============================

.. contents:: Table of Contents

ItemBase
--------

.. autoclass:: save_to_db.core.item_base.ItemBase
    :members:

.. _Item:

Item
----

.. autoclass:: save_to_db.core.item.Item
    :show-inheritance:
    :members:

    .. method:: complete_setup()

        This method validates manual configuration of the item and automatically
        completes configuration based on available data.

ItemMetaclass
-------------

.. autoclass:: save_to_db.core.item_metaclass.ItemMetaclass
    :members:

.. _BulkItem:

BulkItem
--------

.. autoclass:: save_to_db.core.bulk_item.BulkItem
    :show-inheritance:
    :members:

.. _Persister:

Persister
---------

.. autoclass:: save_to_db.core.persister.Persister
    :members:

ItemCollection
--------------
.. autoclass:: save_to_db.core.utils.item_collection.ItemCollection
    :members:

.. _Scope:

Scope
-----

.. autoclass:: save_to_db.core.scope.Scope
    :members:
    :exclude-members: add, remove, clear
    :show-inheritance:

ItemClsManager
---------------

.. automodule:: save_to_db.core.item_cls_manager
    :members:
    :show-inheritance:

selector
--------

.. automodule:: save_to_db.core.utils.selector
    :members:

ModelDeleter
------------

.. automodule:: save_to_db.core.model_deleter
    :members:

ProxyObject
-----------

.. autoclass:: save_to_db.core.utils.proxy_object.ProxyObject
    :show-inheritance:
    :members: