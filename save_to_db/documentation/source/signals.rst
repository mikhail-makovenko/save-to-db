.. _Signals:

Signals
=======

.. contents:: Table of Contents

List of used signals
--------------------

.. automodule:: save_to_db.core.signals
    :members:
    :undoc-members:
    :show-inheritance:

Signal class
------------

.. autoclass:: save_to_db.core.utils.signal.Signal
    :members:
    :undoc-members:
    :show-inheritance:
