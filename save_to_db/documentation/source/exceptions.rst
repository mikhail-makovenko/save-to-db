Exceptions
----------

.. contents:: Table of Contents

Adapter exceptions
++++++++++++++++++
.. automodule:: save_to_db.exceptions.adapter_except
    :members:
    :undoc-members:
    :show-inheritance:

Item class collection exceptions
++++++++++++++++++++++++++++++++
.. automodule:: save_to_db.exceptions.item_collection
    :members:
    :undoc-members:
    :show-inheritance:

Item configuration exceptions
+++++++++++++++++++++++++++++

.. automodule:: save_to_db.exceptions.item_config
    :members:
    :undoc-members:
    :show-inheritance:


Item field usage exceptions
+++++++++++++++++++++++++++++

.. automodule:: save_to_db.exceptions.item_field
    :members:
    :undoc-members:
    :show-inheritance:


Item persistence exceptions
+++++++++++++++++++++++++++++

.. automodule:: save_to_db.exceptions.item_persist
    :members:
    :undoc-members:
    :show-inheritance:

Item process exceptions
+++++++++++++++++++++++++++++

.. automodule:: save_to_db.exceptions.item_process
    :members:
    :undoc-members:
    :show-inheritance:

Merge models policy exceptions
++++++++++++++++++++++++++++++

.. automodule:: save_to_db.exceptions.merge_policy
    :members:
    :undoc-members:
    :show-inheritance:

Scope exceptions
++++++++++++++++

.. automodule:: save_to_db.exceptions.scope_except
    :members:
    :undoc-members:
    :show-inheritance:
