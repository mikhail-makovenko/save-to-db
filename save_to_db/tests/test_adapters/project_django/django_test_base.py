from django.test import TestCase

from save_to_db.utils.test_base import TestBase


class DjangoTestBase(TestBase, TestCase):
    pass
